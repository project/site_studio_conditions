CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Acquia Site Studio Conditions are condition plugins that will evaluate a
node and evaluate different conditions against selected layout canvas
fields.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/site_studio_conditions

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/site_studio_conditions


REQUIREMENTS
------------

This module requires Acquia Site Studio and Context.


INSTALLATION
------------

* Install the module as you would normally install a
  contributed Drupal module.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module along with
       Context and its submodule Context UI.
    2. Navigate to Administration > Structure > Context.
    3. Select "Add context" to add general details for a new context. Save.
    4. Add one of the Layout Canvas conditions and configure it.
    5. Save and continue.

Don't forget to add other conditions to your context so it is not triggered
site-wide (for example, limit to theme, limit to content type or route).

MAINTAINERS
-----------

Current maintainers:
* Kevin Quillen (kevinquillen) - https://kevinquillen.com

Supporting organizations:
* Velir - https://www.drupal.org/velir
