<?php

declare(strict_types=1);

namespace Drupal\site_studio_conditions\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Provides a 'template is active' condition.
 *
 * @Condition(
 *   id = "template_is_active",
 *   label = @Translation("Active Site Studio Template"),
 *   context_definitions = {
 *     "node" = @ContextDefinition(
 *       "entity:node",
 *        label = @Translation("Node")
 *      )
 *   }
 * )
 */
class TemplateIsActive extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->logger = $loggerChannelFactory->get('php');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $configuration = $this->getConfiguration();

    if (!empty($configuration['entity_type_id']) && !empty($configuration['entity_bundle']) && !empty($configuration['entity_field'])) {
      return $this->t('Template field "@field" is checking for the "@template" template.',
        [
          '@field' => $configuration['entity_field'],
          '@template' => $configuration['template'],
        ]
      );
    }
    else {
      return $this->t('No field or template selected.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entity_field' => '',
      'template' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    $node_bundles = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $fields = [];
    $options = [];

    foreach ($node_bundles as $bundle) {
      $fields += $this->getEntityFields('node', $bundle->id(), $bundle->label());
    }

    if (empty($fields)) {
      $form['message'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => $this->t('Before you can configure this condition you must assign a template field from Site Studio to one or more node types.'),
      ];

      return $form;
    }

    $templates = $this->entityTypeManager->getStorage('cohesion_content_templates')->loadMultiple();

    /** @var \Drupal\cohesion_templates\Entity\ContentTemplates $template */
    foreach ($templates as $template) {
      if ($template->get('entity_type') !== 'node') {
        continue;
      }

      $options[$template->getOriginalId()] = $template->label();
    }

    if (empty($options)) {
      $form['message'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => $this->t('Before you can configure this condition you must create Site Studio templates and assign the Template field on content types.'),
      ];

      return $form;
    }

    asort($options);

    $form['entity_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Template field'),
      '#description' => $this->t('Select the template field to evaluate.'),
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $configuration['entity_field'] ?? array_key_first($fields),
    ];

    $form['template'] = [
      '#type' => 'select',
      '#title' => $this->t('Template'),
      '#description' => $this->t('Select the Site Studio template you want to check for.'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $configuration['template'] ?? array_key_first($options),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * Gets all available layout canvas fields for each bundle on an entity type.
   *
   * @param string $entity_type_id
   *   The entity type id, ex. "node".
   * @param string $entity_bundle
   *   The entity bundle, ex. "article".
   * @param string $entity_bundle_label
   *   The entity bundle label, ex. "Article".
   *
   * @return array
   *   An array of layout canvas fields on the entity types.
   */
  protected function getEntityFields(string $entity_type_id, string $entity_bundle, string $entity_bundle_label) : array {
    $bundle_fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $entity_bundle);
    $fields = [];

    /** @var \Drupal\field\Entity\FieldConfig $field */
    foreach ($bundle_fields as $field) {
      if (!$field instanceof FieldConfig) {
        continue;
      }

      if ($field->getType() == 'cohesion_template_selector') {
        $fields[$field->getName()] = $field->label();
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('negate', (bool) $form_state->getValue('negate'));
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['entity_field'] = $values['entity_field'];
    $this->configuration['template'] = $values['template'];
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->context['node']->getContextData()->getValue();

    if (!($node instanceof NodeInterface)) {
      return FALSE;
    }

    $configuration = $this->getConfiguration();
    $field = $configuration['entity_field'];
    $template = $configuration['template'];

    if (!$node->hasField($field)) {
      return FALSE;
    }

    $data = $node->get($field)->getValue();

    if (empty($data)) {
      return FALSE;
    }

    // Workaround to deal with Site Studio naming convention
    if ($data[0]["selected_template"] === '__default__') {
      $value = 'node_' . $node->bundle() . '_full';
    } else {
      $value = $data[0]['selected_template'];
    }

    return ($value === $template);
  }

}
