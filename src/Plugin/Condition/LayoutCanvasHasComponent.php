<?php

declare(strict_types=1);

namespace Drupal\site_studio_conditions\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Provides a 'Layout canvas has component' condition.
 *
 * @Condition(
 *   id = "layout_canvas_has_component",
 *   label = @Translation("Layout canvas has component"),
 *   context_definitions = {
 *     "node" = @ContextDefinition(
 *       "entity:node",
 *        label = @Translation("Node")
 *      )
 *   }
 * )
 */
class LayoutCanvasHasComponent extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->logger = $loggerChannelFactory->get('php');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $configuration = $this->getConfiguration();

    if (!empty($configuration['entity_type_id']) && !empty($configuration['entity_bundle']) && !empty($configuration['entity_field'])) {
      return $this->t('Layout canvas field "@field" on the @entity entity @bundle bundle selected, set to the "@component" component.',
        [
          '@field' => $configuration['entity_field'],
          '@entity' => $configuration['entity_type_id'],
          '@bundle' => $configuration['entity_bundle'],
          '@component' => $configuration['component'],
        ]
      );
    }
    else {
      return $this->t('No field or component selected.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entity_type_id' => '',
      'entity_bundle' => '',
      'entity_field' => '',
      'component' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    if (!empty($configuration['entity_type_id']) && !empty($configuration['entity_bundle']) && !empty($configuration['entity_field'])) {
      $entity_field = $configuration['entity_type_id'] . ':' . $configuration['entity_bundle'] . ':' . $configuration['entity_field'];
    }

    $node_bundles = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $fields = [];
    $components = [];

    foreach ($node_bundles as $bundle) {
      $fields += $this->getEntityFields('node', $bundle->id(), $bundle->label());
    }

    if (empty($fields)) {
      $form['message'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => $this->t('Before you can configure this condition you must assign a layout canvas field from Site Studio to either node types or the user profile.'),
      ];

      return $form;
    }

    $site_studio_components = $this->entityTypeManager->getStorage('cohesion_component')->loadMultiple();

    /** @var \Drupal\cohesion_elements\Entity\Component $component */
    foreach ($site_studio_components as $component) {
      $components[$component->id()] = $component->label();
    }

    if (empty($components)) {
      $form['message'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#markup' => $this->t('Before you can configure this condition you must create Site Studio components.'),
      ];

      return $form;
    }

    asort($components);

    $form['entity_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout canvas field'),
      '#description' => $this->t('Select the Site Studio layout canvas field you want to check.'),
      '#required' => TRUE,
      '#options' => $fields,
      '#default_value' => $entity_field ?? array_key_first($fields),
    ];

    $form['component'] = [
      '#type' => 'select',
      '#title' => $this->t('Component to check'),
      '#description' => $this->t('Select the Site Studio component you want to check for. Note that it is not guaranteed that a particular component exists or is applicable for the layout canvas field selected above.'),
      '#required' => TRUE,
      '#options' => $components,
      '#default_value' => $configuration['component'] ?? array_key_first($components),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * Gets all available layout canvas fields for each bundle on an entity type.
   *
   * @param string $entity_type_id
   *   The entity type id, ex. "node".
   * @param string $entity_bundle
   *   The entity bundle, ex. "article".
   * @param string $entity_bundle_label
   *   The entity bundle label, ex. "Article".
   *
   * @return array
   *   An array of layout canvas fields on the entity types.
   */
  protected function getEntityFields(string $entity_type_id, string $entity_bundle, string $entity_bundle_label) : array {
    $bundle_fields = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $entity_bundle);
    $fields = [];

    /** @var \Drupal\field\Entity\FieldConfig $field */
    foreach ($bundle_fields as $field) {
      if (!$field instanceof FieldConfig) {
        continue;
      }

      $dependencies = $field->getDependencies();

      if (!empty($dependencies['module']) && in_array('cohesion_elements', $dependencies['module'])) {
        $fields[ucfirst($entity_type_id) . ': ' . $entity_bundle_label][$entity_type_id . ':' . $entity_bundle . ':' . $field->getName()] = $field->label();
      }
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $form_state->setValue('negate', (bool) $form_state->getValue('negate'));
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $entity_data = explode(':', $values['entity_field']);
    $this->configuration['entity_type_id'] = $entity_data[0];
    $this->configuration['entity_bundle'] = $entity_data[1];
    $this->configuration['entity_field'] = $entity_data[2];
    $this->configuration['component'] = $values['component'];
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    /** @var \Drupal\node\Entity\Node $node */
    $node = $this->context['node']->getContextData()->getValue();

    if (!($node instanceof NodeInterface)) {
      return FALSE;
    }

    $configuration = $this->getConfiguration();
    $field = $configuration['entity_field'];
    $selected_component = $configuration['component'];

    if (!$node->hasField($field)) {
      return FALSE;
    }

    /** @var \Drupal\cohesion_elements\Entity\CohesionLayout $a */
    $cohesion_data = $node->get($field)->referencedEntities();

    // CohesionLayout is empty.
    if (empty($cohesion_data)) {
      return FALSE;
    }

    $item = $cohesion_data[0];
    $json = json_decode($item->getJsonValues());

    // Layout canvas is empty.
    if (empty($json->canvas)) {
      return FALSE;
    }

    // Selected component is in the list of used components in the canvas.
    foreach ($json->canvas as $component) {
      if ($component->type == 'component' && $component->componentId == $selected_component) {
        return TRUE;
      }
    }

    // Layout canvas lacks the component.
    return FALSE;
  }

}
